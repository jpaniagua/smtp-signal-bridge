# smtp-signal-bridge

You need signal-cli-rest-api:

```sh
docker run -p 8080:8080 \
    -v $(pwd)/signal-cli-config:/home/.local/share/signal-cli \
    -e 'MODE=normal' bbernhard/signal-cli-rest-api:latest
```

Once it's running you can link the signal-api "device":

http://localhost:8080/v1/qrcodelink?device_name=signal-api-test
