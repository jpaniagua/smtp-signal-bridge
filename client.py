import os
from smtplib import SMTP as Client

port = int(os.getenv('SMTP_PORT') or '587')

client = Client('localhost', port)

signal_account = os.getenv('SIGNAL_ACCOUNT')
if (not signal_account):
  raise ValueError('signal account is missing')

signal_recipient = os.getenv('SIGNAL_RECIPIENT') or signal_account

r = client.sendmail(
  f'{signal_account}@signal.bridge',
  [f'{signal_recipient}@signal.bridge'],
  """\
From: Sender Person <{signal_account}@signal.bridge>
To: Recipient Person <{signal_recipient}@signal.bridge>
Subject: A test
Message-ID: <ant>

Hi Bart, this is Anne.
""")

client.quit()
