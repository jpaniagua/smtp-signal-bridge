import asyncio
import email
import httpx
import logging
import os

from aiosmtpd.controller import UnthreadedController

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.INFO)

port = int(os.getenv('SMTP_PORT') or '587')
scra_url_root = os.getenv('SIGNAL_CLI_REST_API_URL_ROOT') or 'http://localhost:8080'

signal_account = os.getenv('SIGNAL_ACCOUNT')
if (not signal_account):
    raise ValueError('signal account is missing')


class CustomHandler:
    def __init__(self, signal_account, queue):
        self.signal_account = signal_account
        self.queue = queue

    async def handle_RCPT(self, server, session, envelope, address, rcpt_options):
        envelope.rcpt_tos.append(address)
        return '250 OK'

    async def handle_DATA(self, server, session, envelope):
        peer = session.peer
        mail_from = envelope.mail_from
        rcpt_tos = envelope.rcpt_tos

        msg = email.message_from_bytes(envelope.content)

        if msg.is_multipart():
            for part in msg.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))

                # skip any text/plain (txt) attachments
                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    body = part.get_payload(decode=True)  # decode
                    break
        # not multipart - i.e. plain text, no attachments, keeping fingers crossed
        else:
            body = msg.get_payload(decode=True)

        signal_message_lines = []

        tokenized_from = msg['From'].split(' ')
        sender_name = ' '.join(tokenized_from[:-1])
        signal_sender = mail_from.split('@')[0]
        if sender_name:
            signal_message_lines.append(f'📨️ {sender_name}\n')

        subject, subject_encoding = email.header.decode_header(msg['Subject'])[0]
        if subject_encoding:
            subject = subject.decode(subject_encoding)

        signal_message_lines.append(f'🏷️ {subject or "(no subject)"}\n')
        signal_message_lines.append(body.decode("utf-8"))
        signal_message = '\n'.join(signal_message_lines)

        signal_recipients = [rcpt_to.split('@')[0] for rcpt_to in rcpt_tos]

        self.queue.put_nowait({
            'message': signal_message,
            'recipients': signal_recipients,
        })
        return '250 Message accepted for delivery'


async def worker(name, queue):
    while True:
        signal_send = await queue.get()
        message = None

        try:
            async with (
                asyncio.timeout(10),
                httpx.AsyncClient() as client
            ):
                r = await client.post(
                    f'{scra_url_root}/v2/send',
                    json={
                        'number': signal_account,
                        'message': signal_send['message'],
                        'recipients': signal_send['recipients'],
                    }
                )

            if (r.status_code >= 500):
                message = f'upstream error: {r.text}'

            elif (r.status_code >= 400):
                message = f'bad request: {r.text}'

        except TimeoutError:
            message = 'timeout waiting for response'

        finally:
            queue.task_done()

        if message:
            logger.error(f'{name}: {message}')
        else:
            logger.info(f'{name}: message sent')


class CustomController(UnthreadedController):
    def __init__(self, handler, *, hostname, port, loop, auth_exclude_mechanism, queue):
        super().__init__(
            handler,
            hostname=hostname,
            port=port,
            loop=loop,
            auth_exclude_mechanism=auth_exclude_mechanism,
        )
        self.queue=queue

        self.worker_tasks = []
        for i in range(3):
            worker_task = loop.create_task(worker(f'worker-{i}', queue))
            self.worker_tasks.append(worker_task)

    async def finalize(self):
        await super().finalize()

        await self.queue.join()

        for worker_task in self.worker_tasks:
            worker_task.cancel()
        # Wait until all worker tasks are cancelled.
        await asyncio.gather(*self.worker_tasks, return_exceptions=True)

 
if __name__ == "__main__":
    logger.info('server init')
    loop = asyncio.get_event_loop()
    queue = asyncio.Queue()

    handler = CustomHandler(signal_account=signal_account, queue=queue)
    controller = CustomController(
        handler,
        hostname='0.0.0.0',
        port=port,
        loop=loop,
        auth_exclude_mechanism=['LOGIN', 'PLAIN'],
        queue=queue,
    )

    controller.begin()
    logger.info(f'server begin at localhost:{port}')
    logger.info(f'- signal account: {handler.signal_account}')

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logger.info('received SIGINT. Terminating...')

    controller.end()

    logger.info('server end')
